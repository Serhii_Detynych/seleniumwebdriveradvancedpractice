package org.example.pages.google.google_cloud.pricing_calculator_dropdowns;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProvisioningModelDropdown extends BaseDropdown {
    @FindBy(xpath = "//md-option[@id='select_option_115']")
    private WebElement regularModel;

    public WebElement getRegularModel() {
        return regularModel;
    }
}
