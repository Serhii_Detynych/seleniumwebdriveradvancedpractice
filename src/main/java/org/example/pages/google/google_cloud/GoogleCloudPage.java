package org.example.pages.google.google_cloud;

import org.example.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleCloudPage extends BasePage {
    @FindBy(xpath = "//div[@class='E65gw lDWUde']")
    private WebElement searchBtn;
    @FindBy(xpath = "//input[@placeholder='Search']")
    private WebElement searchForm;

    public SearchResultPage searchByKeyWord(final String keyWord) {
        waitVisibilityOfElement(10, searchBtn);
        clickOnSearchBtn();
        enterTextInSearchField(keyWord);
        return new SearchResultPage();
    }

    private void clickOnSearchBtn() {
        searchBtn.click();
    }

    private void enterTextInSearchField(final String textForSearch) {
        searchForm.sendKeys(textForSearch + Keys.ENTER);
    }


}
