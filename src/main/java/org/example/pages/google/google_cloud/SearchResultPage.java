package org.example.pages.google.google_cloud;

import org.example.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends BasePage {
    @FindBy(xpath = "//a[contains(text(),'Calculator')]")
    private WebElement pricingCalculator;

    public PricingCalculatorPage clickOnPricingCalculator() {
        waitVisibilityOfElement(10, pricingCalculator);
        pricingCalculator.click();
        return new PricingCalculatorPage();
    }
}
