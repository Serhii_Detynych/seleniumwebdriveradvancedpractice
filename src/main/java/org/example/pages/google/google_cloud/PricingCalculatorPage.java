package org.example.pages.google.google_cloud;

import org.example.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PricingCalculatorPage extends BasePage {

    @FindBy(xpath = "//button[contains(@class,'UywwFc')]")
    private WebElement addToEstimateBtn;

    @FindBy(xpath = "//div[@jsname='rZHESd']")
    private WebElement addToEstimatePopup;

    @FindBy(xpath = "//a[contains(@href,'calculator-legacy')]")
    private WebElement calculatorLegacyLink;

    public void clickOnAddToEstimateBtn() {
        addToEstimateBtn.click();
    }

    public PricingCalculatorLegacyPage clickOnCalculatorLegacyLink() {
        waitVisibilityOfElement(10, addToEstimatePopup);
        setLinkPropertyToOpenInCurrentTab(calculatorLegacyLink);
        calculatorLegacyLink.click();
        return new PricingCalculatorLegacyPage();
    }

}
