package org.example.pages.google.google_cloud.pricing_calculator_dropdowns;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MachineTypeDropdown extends BaseDropdown {
    @FindBy(xpath = "//md-option[@id='select_option_474']")
    private WebElement n1Standard8;

    public WebElement getN1Standard8() {
        return n1Standard8;
    }
}
