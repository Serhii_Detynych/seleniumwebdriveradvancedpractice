package org.example.pages.google.google_cloud.pricing_calculator_dropdowns;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OperatingSystemDropdown extends BaseDropdown {
    @FindBy(xpath = "//md-option[@id='select_option_102']")
    private WebElement freeSystem;

    public WebElement getFreeSystem() {
        return freeSystem;
    }
}
