package org.example.pages.google.google_cloud;

import org.example.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EmailYourEstimatePopup extends BasePage {
    @FindBy(xpath = "//input[@type='email']")
    private WebElement emailField;
    @FindBy(xpath = "//form[@name='emailForm']//button[contains(@class,'md-raised')]")
    private WebElement sendEmailBtn;

    public void enterEmailInEmailField(final String email) {
        emailField.sendKeys(email);
    }

    public void clickOnSendEmailBtn() {
        sendEmailBtn.click();
    }
}
