package org.example.pages.google.google_cloud.pricing_calculator_dropdowns;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CommittedUsageDropdown extends BaseDropdown {
    @FindBy(xpath = "//md-option[@id='select_option_138']")
    private WebElement oneYear;

    public WebElement getOneYear() {
        return oneYear;
    }
}
