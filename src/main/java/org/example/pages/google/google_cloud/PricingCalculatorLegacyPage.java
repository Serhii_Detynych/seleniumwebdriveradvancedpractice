package org.example.pages.google.google_cloud;

import org.example.pages.BasePage;
import org.example.pages.google.google_cloud.pricing_calculator_dropdowns.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PricingCalculatorLegacyPage extends BasePage {
    public static final int TIME_TO_WAIT = 10;
    @FindBy(xpath = "//md-tab-item/div[@title='Compute Engine']")
    private WebElement computeEngine;
    @FindBy(xpath = "//input[@id='input_100']")
    private WebElement numOfInstancesField;
    @FindBy(xpath = "//md-select[@id='select_113']")
    private WebElement operatingSystemAndSoftware;
    @FindBy(xpath = "//md-select[@id='select_117']")
    private WebElement provisioningModel;
    @FindBy(xpath = "//md-select[@id='select_123']")
    private WebElement machineFamily;
    @FindBy(xpath = "//md-select[@id='select_125']")
    private WebElement series;
    @FindBy(xpath = "//md-select[@id='select_127']")
    private WebElement machineType;
    @FindBy(xpath = "//md-select[@id='select_133']")
    private WebElement datacenterLocation;
    @FindBy(xpath = "//md-select[@id='select_140']")
    private WebElement committedUsage;
    @FindBy(xpath = "//button[contains(text(),'Add to Estimate')]")
    private List<WebElement> addToEstimateBtns;
    @FindBy(xpath = "//h2//b[@class='ng-binding']")
    private WebElement totalEstimatedCost;
    @FindBy(xpath = "//button[@id='Email Estimate']")
    private WebElement emailEstimateBtn;


    public void clickOnComputeEngine() {
        computeEngine.click();
    }

    public void enterNumOfInstances(final String number) {
        numOfInstancesField.sendKeys(number);
    }

    public void clickOnOperationSystemAndSoftware() {
        operatingSystemAndSoftware.click();
    }

    public void choseFreeSystemOption() {
        clickOnOperationSystemAndSoftware();
        new OperatingSystemDropdown().getFreeSystem().click();
    }

    public void clickOnProvisioningModel() {
        provisioningModel.click();
    }

    public void choseRegularModelOption() {
        clickOnProvisioningModel();
        new ProvisioningModelDropdown().getRegularModel().click();
    }

    public void clickOnMachineFamily() {
        machineFamily.click();
    }

    public void choseGeneralPurposeOption() {
        clickOnMachineFamily();
        new MachineFamilyDropdown().getGeneralPurpose().click();
    }

    public void clickOnSeries() {
        series.click();
    }

    public void choseN1Option() {
        clickOnSeries();
        waitVisibilityOfElement(TIME_TO_WAIT, new SeriesDropdown().getN1());
        new SeriesDropdown().getN1().click();
    }

    public void clickOnMachineType() {
        machineType.click();
    }

    public void choseN1Standard8Option() {
        clickOnMachineType();
        waitVisibilityOfElement(TIME_TO_WAIT, new MachineTypeDropdown().getN1Standard8());
        new MachineTypeDropdown().getN1Standard8().click();
    }

    public void clickOnDatacenterLocation() {
        datacenterLocation.click();
    }

    public void choseFrankfurtOption() {
        clickOnDatacenterLocation();
        waitVisibilityOfElement(TIME_TO_WAIT, new DatacenterLocationDropdown().getFrankfurt());
        new DatacenterLocationDropdown().getFrankfurt().click();
    }

    public void clickOnCommittedUsage() {
        committedUsage.click();
    }

    public void choseOneYearOption() {
        clickOnCommittedUsage();
        waitVisibilityOfElement(TIME_TO_WAIT, new CommittedUsageDropdown().getOneYear());
        new CommittedUsageDropdown().getOneYear().click();
    }

    public void clickOnFirstAddToEstimateBtn() {
        addToEstimateBtns.getFirst().click();
    }

    public String getTotalEstimatedCost() {
        return totalEstimatedCost.getText();
    }

    public EmailYourEstimatePopup clickOnEmailEstimateBtn() {
        emailEstimateBtn.click();
        return new EmailYourEstimatePopup();
    }
}
