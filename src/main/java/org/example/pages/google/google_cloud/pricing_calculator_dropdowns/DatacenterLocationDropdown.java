package org.example.pages.google.google_cloud.pricing_calculator_dropdowns;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DatacenterLocationDropdown extends BaseDropdown {

    @FindBy(xpath = "//md-option[@id='select_option_268']")
    private WebElement frankfurt;

    public WebElement getFrankfurt() {
        return frankfurt;
    }
}
