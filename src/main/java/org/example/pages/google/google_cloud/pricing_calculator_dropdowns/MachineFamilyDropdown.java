package org.example.pages.google.google_cloud.pricing_calculator_dropdowns;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MachineFamilyDropdown extends BaseDropdown {
    @FindBy(xpath = "//md-option[@id='select_option_119']")
    private WebElement generalPurpose;

    public WebElement getGeneralPurpose() {
        return generalPurpose;
    }
}
