package org.example.pages.google.google_cloud.pricing_calculator_dropdowns;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SeriesDropdown extends BaseDropdown {
    @FindBy(xpath = "//md-option[@id='select_option_224']")
    private WebElement n1;

    public WebElement getN1() {
        return n1;
    }
}
