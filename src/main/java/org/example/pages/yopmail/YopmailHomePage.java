package org.example.pages.yopmail;

import org.example.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YopmailHomePage extends BasePage {
    @FindBy(xpath = "//input[@id='login']")
    private WebElement loginInputField;

    @FindBy(xpath = "//div[@id='refreshbut']")
    private WebElement enterEmailBtn;

    public void enterTextIntoInputField(final String email) {
        loginInputField.sendKeys(email);
    }

    public YopmailMailPage clickOnEnterEmailBtn() {
        enterEmailBtn.click();
        return new YopmailMailPage();
    }
}
