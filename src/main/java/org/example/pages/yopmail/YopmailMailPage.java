package org.example.pages.yopmail;

import org.example.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YopmailMailPage extends BasePage {
    @FindBy(xpath = "//div[@class='bname']")
    private WebElement emailName;
    @FindBy(xpath = "//h3[contains(text(),'USD')]")
    private WebElement estimatedCost;

    public String getEmailName() {
        return emailName.getText();
    }

    public String getEstimatedCost() {
        return estimatedCost.getText();
    }
}
