package org.example.pages;

import org.example.core.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
    protected WebDriver driver;

    public BasePage() {
        this.driver = DriverManager.getInstance();
        PageFactory.initElements(driver, this);
    }

    public void waitVisibilityOfElement(long timeToWait, WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeToWait));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void setLinkPropertyToOpenInCurrentTab(WebElement link) {
        ((org.openqa.selenium.JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('target','_self')", link);
    }
}
