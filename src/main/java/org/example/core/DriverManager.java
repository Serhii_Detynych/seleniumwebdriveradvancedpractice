package org.example.core;

import org.example.factories.webdriver_factory.WebDriverFactory;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class DriverManager {

    private static String defaultBrowser = "chrome";
    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    private DriverManager() {
    }

    public static WebDriver getInstance() {
        if (driver.get() == null) {
            driver.set(WebDriverFactory.createWebDriver(defaultBrowser));
        }
        return driver.get();
    }

    public static void setDefaultBrowser(String browser) {
        defaultBrowser = browser;
    }

    public static void quitDriver() {
        if (driver.get() != null) {
            driver.get().quit();
            driver.remove();
        }
    }

    public static void setBrowserWindowMaxSize() {
        getInstance().manage().window().maximize();
    }

    public static void setDriverImplicitlyWait(int seconds){
        getInstance().manage().timeouts().implicitlyWait(Duration.ofSeconds(seconds));
    }
}
