package org.example.logger;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

public class BasicWatcher implements TestWatcher {
    @Override
    public void testSuccessful(ExtensionContext context) {
        System.out.println("Test successful: " + context.getDisplayName());
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        System.out.println("Test failed: " + context.getDisplayName());
    }
}
