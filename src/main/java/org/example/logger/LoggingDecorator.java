package org.example.logger;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

public class LoggingDecorator implements TestWatcher {
    private final TestWatcher watcher;
    public LoggingDecorator() {
        this.watcher = new BasicWatcher();
    }

    public LoggingDecorator(TestWatcher watcher) {
        this.watcher = watcher;
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        System.out.println("Logging: Test successful: " + context.getDisplayName());
        watcher.testSuccessful(context);
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        System.out.println("Logging: Test failed: " + context.getDisplayName());
        watcher.testFailed(context, cause);
    }
}
