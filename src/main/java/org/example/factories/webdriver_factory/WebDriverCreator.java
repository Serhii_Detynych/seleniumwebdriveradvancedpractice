package org.example.factories.webdriver_factory;

import org.openqa.selenium.WebDriver;

public interface WebDriverCreator {
    WebDriver createWebDriver();
}
