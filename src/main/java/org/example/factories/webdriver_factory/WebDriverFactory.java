package org.example.factories.webdriver_factory;

import org.openqa.selenium.WebDriver;

public class WebDriverFactory {
    static WebDriverCreator webDriverCreator;

    public static WebDriver createWebDriver(String browser) {
        if (browser.equalsIgnoreCase("chrome")) {
            webDriverCreator = new ChromeDriverCreator();
            return webDriverCreator.createWebDriver();
        } else if (browser.equalsIgnoreCase("firefox")) {
            webDriverCreator = new FirefoxDriverCreator();
            return webDriverCreator.createWebDriver();
        } else {
            throw new IllegalArgumentException("Unsupported browser: " + browser);
        }
    }
}
