import org.example.core.DriverManager;
import org.example.logger.LoggingDecorator;
import org.example.pages.google.google_cloud.*;
import org.example.pages.yopmail.YopmailHomePage;
import org.example.pages.yopmail.YopmailMailPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;

@ExtendWith(LoggingDecorator.class)
public class PricingCalculatorTest {

    private static final String GOOGLE_CLOUD_LINK = "https://cloud.google.com/";
    private static final String SEARCH_KEY_WORD = "Google Cloud Platform Pricing Calculator";
    private static final String YOPMAIL_LINK = "https://yopmail.com/";
    private static final String EMAIL_PREFIX = "sd_seleniumtest";
    private static WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = DriverManager.getInstance();
        DriverManager.setBrowserWindowMaxSize();
        DriverManager.setDriverImplicitlyWait(10);
    }

    @Test
    public void pricingCalculatorTest() {
        String email = createMail(EMAIL_PREFIX);

        String expectedResult = getTotalEstimatedCostFromCalculator();

        sendEstimationByEmail(email);

        String estimatedCost = getTotalEstimateFromEmail();

        Assertions.assertTrue(expectedResult.contains(estimatedCost));
    }

    @AfterEach
    public void tearDown() {
        DriverManager.quitDriver();
    }

    private String getTotalEstimateFromEmail() {
        driver.get(YOPMAIL_LINK);
        YopmailHomePage yopmailHomePage = new YopmailHomePage();
        YopmailMailPage yopmailMailPage = yopmailHomePage.clickOnEnterEmailBtn();

        driver.switchTo().frame("ifmail");
        return yopmailMailPage.getEstimatedCost();
    }

    private static void sendEstimationByEmail(String email) {
        EmailYourEstimatePopup emailYourEstimatePopup = new PricingCalculatorLegacyPage().clickOnEmailEstimateBtn();

        emailYourEstimatePopup.enterEmailInEmailField(email);
        emailYourEstimatePopup.clickOnSendEmailBtn();
    }

    private String getTotalEstimatedCostFromCalculator() {
        PricingCalculatorLegacyPage pricingCalculatorLegacyPage = navigateToPricingCalculatorLegacyPage();

        switchToPricingCalculatorFrame();

        fillComputeEngineCalculator(pricingCalculatorLegacyPage);

        pricingCalculatorLegacyPage.clickOnFirstAddToEstimateBtn();

        return pricingCalculatorLegacyPage.getTotalEstimatedCost();
    }

    private PricingCalculatorLegacyPage navigateToPricingCalculatorLegacyPage() {
        driver.get(GOOGLE_CLOUD_LINK);

        GoogleCloudPage googleCloudPage = new GoogleCloudPage();

        SearchResultPage searchResultPage = googleCloudPage.searchByKeyWord(SEARCH_KEY_WORD);

        PricingCalculatorPage pricingCalculatorPage = searchResultPage.clickOnPricingCalculator();
        pricingCalculatorPage.clickOnAddToEstimateBtn();
        return pricingCalculatorPage.clickOnCalculatorLegacyLink();
    }

    private String createMail(final String email) {
        driver.get(YOPMAIL_LINK);
        YopmailHomePage yopmailHomePage = new YopmailHomePage();
        yopmailHomePage.enterTextIntoInputField(email);
        YopmailMailPage yopmailMailPage = yopmailHomePage.clickOnEnterEmailBtn();
        return yopmailMailPage.getEmailName();
    }

    private static void fillComputeEngineCalculator(PricingCalculatorLegacyPage pricingCalculatorLegacyPage) {
        pricingCalculatorLegacyPage.clickOnComputeEngine();
        pricingCalculatorLegacyPage.enterNumOfInstances("4");
        pricingCalculatorLegacyPage.choseFreeSystemOption();
        pricingCalculatorLegacyPage.choseRegularModelOption();
        pricingCalculatorLegacyPage.choseGeneralPurposeOption();
        pricingCalculatorLegacyPage.choseN1Option();
        pricingCalculatorLegacyPage.choseN1Standard8Option();
        pricingCalculatorLegacyPage.choseFrankfurtOption();
        pricingCalculatorLegacyPage.choseOneYearOption();
    }

    private void switchToPricingCalculatorFrame() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(0);
    }
}
